from flask import Flask, jsonify, request
from neo4j import GraphDatabase
from dotenv import load_dotenv
import os

app = Flask(__name__)

load_dotenv()

uri = os.getenv("URI")
user = os.getenv("USERNAME")
password = os.getenv("PASSWORD")
driver = GraphDatabase.driver(uri, auth=(user, password), database="neo4j")


def get_all_employees(tx, filter_by=None, order_by=None):
    query = "MATCH (n:Employee)"

    if filter_by:
        query += (
            f" WHERE n.name CONTAINS '{filter_by}' OR n.surname CONTAINS '{filter_by}'"
        )

    query += " RETURN id(n) as id, n.name as name, n.surname as surname, n.position as position"

    if order_by:
        query += f" ORDER BY n.{order_by}"

    results = tx.run(query).data()
    employees = [
        {
            "id": result["id"],
            "name": result["name"],
            "surname": result["surname"],
            "position": result["position"],
        }
        for result in results
    ]
    return employees


def check_employee_uniqueness(tx, name, surname):
    query = "MATCH (e:Employee) WHERE e.name = $name AND e.surname = $surname RETURN COUNT(e) AS count"
    result = tx.run(query, name=name, surname=surname).single()
    return result["count"] == 0


def add_employee(tx, name, surname, position, department):
    existing_department_query = "MATCH (d:Department {deptName: $department}) RETURN d"
    existing_department = tx.run(
        existing_department_query, department=department
    ).single()

    if existing_department:
        if position.lower() == "manager":
            query = (
                "MATCH (d:Department {deptName: $department}) "
                "CREATE (e:Employee {name: $name, surname: $surname, position: $position})-[:WORKS_IN]->(d), "
                "(e)-[:MANAGES]->(d)"
            )
        else:
            query = "MATCH (d:Department {deptName: $department}) CREATE (e:Employee {name: $name, surname: $surname, position: $position})-[:WORKS_IN]->(d)"
    else:
        if position.lower() == "manager":
            query = (
                "CREATE (e:Employee {name: $name, surname: $surname, position: $position})-[:WORKS_IN]->(d:Department {deptName: $department}), "
                "(e)-[:MANAGES]->(d)"
            )
        else:
            query = "CREATE (e:Employee {name: $name, surname: $surname, position: $position})-[:WORKS_IN]->(d:Department {deptName: $department})"

    tx.run(query, name=name, surname=surname, position=position, department=department)


def edit_employee_helper(
    tx, new_firstname, new_lastname, new_department, new_stanowisko, id
):
    if new_stanowisko.lower() == "manager":
        query = f"""
        MATCH (e:Employee)-[r]->(d:Department)
        WHERE id(e) = {id}
        DELETE r
        WITH e
        MATCH (d2:Department {{name: '{new_department}'}})
        CREATE (e)-[:MANAGES]->(d2)
        SET e.name = '{new_firstname}'
        SET e.surname = '{new_lastname}'
        SET e.position = '{new_stanowisko}'
        """
        tx.run(query)
    else:
        query = f"""
        MATCH (e:Employee)-[r]->(d:Department)
        WHERE id(e) = {id}
        DELETE r
        WITH e
        MATCH (d2:Department {{name: '{new_department}'}})
        CREATE (e)-[:WORKS_IN]->(d2)
        SET e.name = '{new_firstname}'
        SET e.surname = '{new_lastname}'
        SET e.position = '{new_stanowisko}'
        """
    tx.run(query)


def remove_manages_in_relation(tx, employee_id):
    query = (
        "MATCH (e:Employee)-[r:MANAGES]->(d:Department) WHERE id(e) = $employee_id "
        "DELETE r"
    )
    tx.run(query, employee_id=employee_id)


def get_subordinates_workplace(tx, employee_id):
    query = (
        "MATCH (manager:Employee)-[:MANAGES]->(:Department)<-[:WORKS_IN]-(subordinate:Employee) "
        "WHERE id(manager) = $employee_id "
        "RETURN id(subordinate) as id, subordinate.name as name, subordinate.surname as surname, subordinate.position as position"
    )
    results = tx.run(query, employee_id=employee_id).data()
    subordinates = [
        {
            "id": result["id"],
            "name": result["name"],
            "surname": result["surname"],
            "position": result["position"],
        }
        for result in results
    ]
    return subordinates


def remove_employee(tx, employee_id):
    query = "MATCH (e:Employee) WHERE id(e) = $employee_id DETACH DELETE e"
    tx.run(query, employee_id=employee_id)


def remove_department(tx, department_id):
    query = "MATCH (d:Department) WHERE id(d) = $department_id DETACH DELETE d"
    tx.run(query, department_id=department_id)


def assign_new_manager(tx, department_id, new_manager_id):
    query = (
        "MATCH (d:Department)<-[r:WORKS_IN]-(e:Employee) "
        "WHERE id(d) = $department_id"
        "CREATE (e)-[:MANAGES]->(d))"
    )
    tx.run(query, department_id=department_id, new_manager_id=new_manager_id)


def get_employees_in_department_by_name(tx, department_name):
    query = (
        "MATCH (d:Department {deptName: $department_name})<-[:WORKS_IN]-(e:Employee) "
        "RETURN id(e) as id, e.name as name, e.surname as surname, e.position as position"
    )
    results = tx.run(query, department_name=department_name).data()
    employees = [
        {
            "id": result["id"],
            "name": result["name"],
            "surname": result["surname"],
            "position": result["position"],
        }
        for result in results
    ]
    return employees


def get_all_departaments(tx, filter_by=None, order_by=None):
    query = "MATCH (n:Department)"

    if filter_by:
        query += f" WHERE n.deptName CONTAINS '{filter_by}'"

    query += " RETURN id(n) as id, n.deptName as deptName"

    if order_by:
        query += f" ORDER BY n.{order_by}"

    results = tx.run(query).data()
    departments = [
        {
            "id": result["id"],
            "deptName": result["deptName"],
        }
        for result in results
    ]
    return departments


@app.route("/employees", methods=["GET"])
def get_all_employees_route():
    filter_by = request.args.get("filter_by")
    order_by = request.args.get("order_by")

    with driver.session() as session:
        employees = session.read_transaction(get_all_employees, filter_by, order_by)
    response = {"employees": employees}
    return jsonify(response), 200


@app.route("/employees", methods=["POST"])
def add_new_employee_route():
    data = request.get_json()

    required_fields = ["name", "surname", "position", "department"]
    for field in required_fields:
        if field not in data:
            return jsonify({"error": f"Field '{field}' is required"}), 400

    with driver.session() as session:
        if not session.read_transaction(
            check_employee_uniqueness, data["name"], data["surname"]
        ):
            return (
                jsonify(
                    {"error": "Employee with the same name and surname already exists"}
                ),
                400,
            )

        session.write_transaction(
            add_employee,
            data["name"],
            data["surname"],
            data["position"],
            data["department"],
        )

    return jsonify({"message": "Employee added successfully"}), 201


@app.route("/employees/<int:id>", methods=["PUT"])
def edit_employee(id):
    new_name = request.json["name"]
    new_lastname = request.json["surname"]
    new_department = request.json["department"]
    new_stanowisko = request.json["position"]

    with driver.session() as session:
        session.write_transaction(
            edit_employee_helper,
            new_name,
            new_lastname,
            new_department,
            new_stanowisko,
            id,
        )
        return jsonify({"message": "Employee edited"}), 201


@app.route("/employees/<int:employee_id>/subordinates", methods=["GET"])
def get_subordinates_route(employee_id):
    with driver.session() as session:
        subordinates = session.read_transaction(get_subordinates_workplace, employee_id)
    response = {"subordinates": subordinates}
    return jsonify(response), 200


@app.route("/employees/<int:employee_id>", methods=["DELETE"])
def remove_employee_route(employee_id):
    with driver.session() as session:
        manager_query = "MATCH (e:Employee)-[:MANAGES]->(d:Department) WHERE id(e) = $employee_id RETURN d, e"
        result = session.run(manager_query, employee_id=employee_id).single()

        if result:
            department_id = result["d"].id
            new_manager_query = (
                "MATCH (d:Department)<-[:WORKS_IN]-(e:Employee) "
                "WHERE id(d) = $department_id AND id(e) <> $employee_id "
                "RETURN e LIMIT 1"
            )
            new_manager_result = session.run(
                new_manager_query, department_id=department_id
            ).single()

            if new_manager_result:
                new_manager_id = new_manager_result["e"].id
                session.write_transaction(
                    assign_new_manager, department_id, new_manager_id
                )
            else:
                session.write_transaction(remove_department, department_id)
        session.write_transaction(remove_employee, employee_id)

    return jsonify({"message": "Employee removed successfully"}), 200


@app.route("/employees/<string:department_name>", methods=["GET"])
def get_employees_in_department(department_name):
    with driver.session() as session:
        employees = session.read_transaction(
            get_employees_in_department_by_name, department_name
        )
    response = {"employees": employees}
    return jsonify(response), 200


@app.route("/departments", methods=["GET"])
def get_all_departments_route():
    filter_by = request.args.get("filter_by")
    order_by = request.args.get("order_by")

    with driver.session() as session:
        departments = session.read_transaction(
            get_all_departaments, filter_by, order_by
        )
    response = {"departments": departments}
    return jsonify(response), 200


if __name__ == "__main__":
    app.run()
